public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact johnDoe = new Contact("John Doe");
        johnDoe.setContactNumber("+639152468596");
        johnDoe.setAddress("my home in Quezon City");
        johnDoe.addContactNumber("+639228547963");
        johnDoe.addAddress("my office in Makati City");

        Contact janeDoe = new Contact("Jane Doe");
        janeDoe.setContactNumber("+639162148573");
        janeDoe.setAddress("my home in Caloocan City");
        janeDoe.addContactNumber("+639173698541");
        janeDoe.addAddress("my office in Pasay City");

        phonebook.addContact(johnDoe);
        phonebook.addContact(janeDoe);

        phonebook.displayContacts();
    }
}