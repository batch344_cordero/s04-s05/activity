import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> contactNumbers;
    private ArrayList<String> addresses;

    public Contact(String name) {
        this.name = name;
        this.contactNumbers = new ArrayList<>();
        this.addresses = new ArrayList<>();
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumbers.add(contactNumber);
    }

    public void addContactNumber(String contactNumber) {
        this.contactNumbers.add(contactNumber);
    }

    public void setAddress(String address) {
        this.addresses.add(address);
    }

    public void addAddress(String address) {
        this.addresses.add(address);
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getContactNumbers() {
        return contactNumbers;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }
}