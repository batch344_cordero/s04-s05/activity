import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook() {
        this.contacts = new ArrayList<>();
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void displayContacts() {
        for (Contact contact : contacts) {
            System.out.println(contact.getName());
            System.out.println("----------");

            System.out.println(contact.getName() + " has the following registered numbers:");
            for (String number : contact.getContactNumbers()) {
                System.out.println(number);
            }
            System.out.println("-------------------------");

            System.out.println(contact.getName() + " has the following registered addresses:");
            for (String address : contact.getAddresses()) {
                System.out.println(address);
            }
            System.out.println("=========================");
        }
    }
}